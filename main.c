#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

bool is_punctuation(char c) {
    return c == ',' || c == '.' || c == '?' || c == '!';
}

bool word_is_punctuation(char *c) {
    return c[0] == ',' || c[0] == '.' || c[0] == '?' || c[0] == '!';
}

void write_word_to_file(char *word, char *last_word) {
    // TODO: Finish this function
}

void reset_array(char *arr, int *n) {
    // Reset an array with n values set in it
    for (int i = 0; i < n; i++) {
        arr[i] = '\0';
    }
    *n = 0;
}

FILE *load_dictionary() {
    FILE *file = fopen("tp_dictionary.txt", "r+");

    if (file != NULL) {
        return file;
    }
    fclose(file);

    file = fopen("tp_dictionary.txt", "w+");
    return file;
}

int get_word_after(char *word[]) {
    // Return the number of characters into the line in which to find the next word in the text
    FILE *file = load_dictionary();

    if (file != NULL) {
        // Initialise the current-word buffer
        int current_word[20];
        for (int i = 0; i < 20; i++) {
            current_word[i] = '\0';
        }

        while (fscanf(file, "\n") != EOF) {
            fscanf(file, "%20s ", current_word);
            if (!strcmp(current_word, word)) {
                // We're at the correct line, now search for the next word
                int buffer[200], total_count = 0, cursor = ftell(file);
                
                fgets(buffer, 200, file);
                // TODO: Calculate total_count

                // Use total_count to determine which occurrence of the next word we should get
                total_count *= (double)rand() / (double)RAND_MAX;
                fseek(file, cursor, SEEK_SET);

                // Get the word at this occurrence number
                int c;
                while ((fscanf(file, "%d", c))) {
                    total_count -= c;
                    if (total_count < 0) {
                        fgetc(file);
                        return ftell(file);
                    }
                }
            }
        }
    }

    return -1;
}

int find_argument_position(int argc, char *argv[], char *target[]) {
    // Obtain the position of the -target command line argument
    // Example use: find_argument_position(argc, argv, "-m")
    int address = -1;
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], target)) {
            // printf("%d %s %s\n",i,argv[i], argv[i+1]);
            address = i + 1;
            break;
        }
    }
    return address;
}

int read(int argc, char *argv[]) {
    int path_index = find_argument_position(argc, argv, "-p");

    if (path_index == argc) {
        printf("Usage:\n-m read -p filepath.txt (for read mode)\n-m predict Start of a story ... (for predict mode)");
    }

    FILE *fd = fopen(argv[path_index], "r");
    FILE *fp = load_dictionary();

    if (fd != NULL) {
        char word[20], last_word[20];
        char c;
        int pos = 0;

        while ((c = getc(fd)) != EOF) {
            if (is_punctuation(c)) {
                // TODO: Test to ensure that the pointers are accurate
                write_word_to_file(word, last_word);
                word[0] = c, word[1] = '\0';
                write_word_to_file(c, word);
                reset_array(word, &pos);
            }

            else if (c == ' ') {
                write_word_to_file(word, last_word);
                for (int i = 0; i < pos; i++)
                    last_word[i] = word[i];
                reset_array(word, &pos);
            }

            else {
                word[pos] = c;
                pos++;
            }
        }
    }

    fprintf(fp, "Hello world");

    fclose(fd);
    fclose(fp);
}

int predict(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Usage:\n-m read -p filepath.txt (for read mode)\n-m predict Start of a story ... (for predict mode)");
    }

    // Open the dictionary file
    FILE *file = load_dictionary();

    // Current word buffer
    char *word[20];
    strlcpy(word, argv[argc-1], 20);

    // Print the string that's already been submitted
    for (int i = i; i < argc; i++) {
        printf("%s ", argv[i]);
    }

    // Copy the remaining words
    int cur = get_word_after(word);
    fseek(file, cur, SEEK_SET);
    fscanf(file, "%s ", &word);
    printf(word);
    rewind(file);

    while ((cur = get_word_after(word)) != -1) {
        fseek(file, cur, SEEK_SET);
        fscanf(file, "%s ", &word);
        if (word_is_punctuation(word))
            printf(" ");
        printf(word);
        rewind(file);
    }

    return 0;
}

int main(int argc, char *argv[]) {

    int mode_address = find_argument_position(argc, argv, "-m");
    if (mode_address == -1) {
        printf("Usage:\n-m read -p filepath.txt (for read mode)\n-m predict Start of a story ... (for predict mode)");
    }

    else {
        if (!strcmp(argv[mode_address], "read")) {
            read(argc, argv);
        }

        if (!strcmp(argv[mode_address], "predict")) {
            predict(argc, argv);
        }

        else {
            printf("Usage:\n-m read -p filepath.txt (for read mode)\n-m predict Start of a story ... (for predict mode)");
        }
    }

    return 0;
}
