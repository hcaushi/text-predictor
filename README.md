This program was written in C. Its primary purpose was for humour: given a string, it will continue the flow of writing, often with amusing results!

Usage:
This program has two modes: 'read' and 'predict'. To select a mode, use the command line argument -m.
- Read mode allows the program to read from a text file, training it with text data. It should be followed by "-p filename.txt", where filename.txt is a text file.
- Predict mode allows the program to take a string input and predict what string finishes off the input. It should be followed by any number of words, which will be printed again in the output.

Enjoy!
