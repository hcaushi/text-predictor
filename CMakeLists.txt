# cmake_minimum_required(VERSION <specify CMake version here>)
project(Text_Predictor)

set(CMAKE_CXX_STANDARD 14)
set(SOURCE_FILES main.c)

add_executable(Text_Predictor main.c)